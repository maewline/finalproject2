/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.projectfinal2;

/**
 *
 * @author Arthit
 */
public class Human { //สร้าง Class Human และ กำหนด Attribute ที่มี Modifier เป็น Protected เพื่อที่จะให้สืบทอดไปยังคลาสลูกได้ 
   protected String name;
   protected Money money;
   protected Card card;

    public Human(String name, Money money, Card card) { //สร้าง Constuctor และกำหนดค่าลงใน Attribute
        this.name = name;
        this.money = money;
        this.card = card;
    }
   
    public void Hello(){ //สร้าง method hello เพื่อเอาไว้ดูค่า Attribute ต่างๆ ได้แก่ name money and cards
        System.out.println("Hello I am "+name+". Now I have money "+money.volume+" bath. "+"and "+card.volume+" cards.");
    }
    
    public void showSignature(){ // แสดงเอกลักษณ์ประจำตัว
        System.out.println("I'm like a normal person.");
    }
   
}
