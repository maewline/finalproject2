/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.projectfinal2;

/**
 *
 * @author Arthit
 */
public class Yaisa  extends Human{ // Class Yaisa จะ Inheritance กับ คลาส Human เพื่อที่จะเรียกใช้ Attribute หรือ method ต่างๆในคลาสแม่ได้
    private Extra extra; //ประกาศคลาส Extra มาใช้ เพื่อที่จะให้คลาส Yaisa รู้จักกัน และสามารถเรียกใช้ method ของคลาส Extra ได้

    public Yaisa(String name, Money money, Card card) { //สร้าง Constuctor   Yaisa
        super(name, money, card);
    }

    public void setExtra(Extra extra) { //method นี้จำเป็นการ set extra เพื่อที่จะให้คลาส Yaisa รู้จักและเรียกใช้งานจากคลาสยายสาได้
        this.extra = extra;
    }
    
    @Override
    public void showSignature(){  //แสดงเอกลักษณ์ตัวละคร
        System.out.println("Complaning.");
    }
    
    public int getMoneyYaisa (int x){ //method นี้ จะเป็น method ที่เอาไว้ใช้ตอน extra มา ขอเงิน ยายสาใช้ โดยรับเป็นพารามิเตอร์เป็น  x บาทโดย... (Overload method)
        if(x>money.getVolume()){ //ถ้าเงินที่ extra ขอมา มากกว่าเงินของยายสา ก็จะไม่ได้เงินกลับไป 
            System.out.println("My money not enough for you!!");
            return 0;
        }money.decrease(x); //แต่ถ้าไม่ตรงเงื่อนไขด้านบน ก็จะ ใช้ Object money ของยายสา ไปเรียกใช้ method decrease เพื่อลดตังยายสาลง 
        return x; //แล้ว return ไปตามคำขอ
    }
    
    public int getMoneyYaisa (){ //method นี้เป็นการเรียกเงินจากกระเป๋ายายสาเหมือนกัน แต่ไม่มีการรับ พารามิเตอร์ โดยถ้าใช้ method นี้ คือมีเงินเท่าไหร่ เอาไปให้หมดเลย (Overload method)
        int x=0; // ประกาศตัวแปร x
        if(this.money.getVolume()>0){ //เมื่อ เงินในกระเป๋าตังยายมากกว่า 0
            x=this.money.getVolume(); //นำเงินยายไปไว้ใน x
            this.money.volume=0; //ล้างกระเป๋ายายทิ้ง
        }else{
            System.out.println("My money not enough for you!!");
        }
        return x; //รีเทิร์นเงินกลับไป
    }
    
    public void seizeCard(int x){ // method นี้เป็นการ ยึดการ์ดที่่เมื่อเรียกใช้แล้ว จะไปยึดการ์ดของ extra เป็นจำนวน x ใบ (Overload method)
        if(extra.card.volume<x){  //เมื่อการ์ด extra น้อยกว่า จำนวนที่ยายต้องการจะยึด ให้แสดงข้อความเตือนว่า ตอนนี้มีการ์ดไม่เพียงพอ
            System.out.println("Card not enough for you!!");
        }else{
            extra.card.decrease(x); //ถ้าไม่ตรงงเงื่อนไขบน ก็ใช้ Object extra ไปเรียก Object card แล้วไปใช้ method ลดค่าลง  x จำนวน
            card.add(x); // แล้ว ใช้ Object card ของยายสา ไปเรียกใช้ method add เพื่อเพิ่มค่าจำนวนค่า การ์ดของยายสา
        }
    }
    
    public void seizeCard(){ // method นี้เป็นการยึดการเหมือนกัน แต่ไม่มีการรับพารามิเตอร์ (Overload method) ถ้า...
        if(extra.card.volume>0){  //การ์ดของ extra มากกว่า 0 ใบ
            card.add(extra.card.volume); //ให้ Object card ของยายเรียกใช้ method add เพื่อเพิ่มค่าการ์ดของ extra ทั้งหมด
            extra.card.decrease(); //และใช้ Object extra ไปเรียก Object card แล้ว ให้ Object คลาสไปเรียกใช่ method decrease เพื่อลดค่า จำนวนลง
        }else{
            System.out.println("Card not enough for you!!");
        }
    }
    
    public void Complaning(){ //เอกลักษณ์การบ่นของยายสา
        System.out.println("You!! @  !  +   ^   %   #   *   &   @   $   ");
    }
    
    
}
