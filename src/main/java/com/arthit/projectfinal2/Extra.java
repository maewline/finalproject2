/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.projectfinal2;

/**
 *
 * @author Arthit
 */
public class Extra extends Human{ //สร้าง คลาส Extra ขึ้นมา โดยทำการ Inheritance จาก คลาส Human เพื่อเรียกใช้ Attribute หรือ method ต่างๆจากคลาสแม่ได้
    private Yaisa yaisa; //ประกาศคลาสยายสามาใช้ เพื่อที่จะให้คลาส Extra รู้จักกัน และสามารถเรียกใช้ method ของคลาสยายสาได้

    public Extra(String name, Money money, Card card) { //สร้าง Constuctor Extra
        super(name, money, card);
    }

    public void setYaisa(Yaisa yaisa) { //method นี้จำเป็นการ set Yaisa เพื่อที่จะให้คลาส Extra รู้จักและเรียกใช้งานจากคลาสยายสาได้
        this.yaisa = yaisa;
    }
    
    @Override
    public void showSignature(){ //แสดงเอกลักษณ์ตัวละคร
        System.out.println("Sleep in the class.");
    }
    
    public void slepping(){ //เอกลักษณ์ของคลาสนี้
        System.out.println("ZZZzzzzzzz");
    }
    
    public void UseMoney(int x , String name){ // method นี้ จะเป็นการให้ตัวละครนี้ ใช้เงินจากยายสา (Overload method) 
        if(name.equals("yaisa")){
            money.add(yaisa.getMoneyYaisa(x)); //เมื่อเงื่อนไขถูกต้องจะทำการอ้างอิง Object money ของยาย แล้วเรียกใช้ method add เพื่อเพิ่มตังของ extra โดยเรียกใช้ method จากยายสา แล้วส่งค่า x เข้าไป
        }
    }
    
    public void UseMoney(int x ){ // method นี้ จะเป็นการ ใช้เงินของตัวละครเอง (extra) โดยถ้าเงินตัวละครมากกว่า 0 ...
        if(money.volume>0){
            money.decrease(x); // ก็จะเรียกใช้ Object money ของ extra ไปเรียกใช้ method decrease เพื่อลดจำนวนเงินลง
        }else{
            System.out.println("I don't have money T__T");
        }
    }
    
    
}
