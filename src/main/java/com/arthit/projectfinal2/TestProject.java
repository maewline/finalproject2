/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.projectfinal2;

/**
 *
 * @author Arthit
 */
public class TestProject {

    public static void main(String[] args) {
        
        Extra extra = new Extra("Extra", new Money(0), new Card(30)); //สร้าง Object extra ขึ้นมาจากคลาส Extra
        extra.Hello(); //extra ทักทาย

        Yaisa yaisa = new Yaisa("Yaisa", new Money(5000), new Card(0)); //สร้าง Object yaisa ขึ้นมาจากคลาส Yaisa
        yaisa.Hello(); //yaisa ทักทาย

        extra.setYaisa(yaisa);  //set Yaisa เพื่อที่จะให้คลาส Extra รู้จักและเรียกใช้งานจากคลาสยายสาได้
        yaisa.setExtra(extra); //set extra เพื่อที่จะให้คลาส Yaisa รู้จักและเรียกใช้งานจากคลาสยายสาได้

        extra.showSignature(); //แสดงเอกลักษณ์ของตัวละครเอ็กต้า
        yaisa.showSignature();  //แสดงเอกลักษณ์ของตัวละครยายสา

        extra.UseMoney(30, "yaisa"); //ต้าใช้เงินยายสา 30 บาท
        yaisa.seizeCard(10); //ยายสายึดการ์ดต้ามา 10 ใบ
        extra.Hello(); //แสดงผลลัพธ์ต้า
        yaisa.Hello(); //แสดงผลลัพธ์ยายสา

        extra.UseMoney(10); //ต้าใช้เงินไป 10 บาท 
        extra.Hello(); //แสดงผลลัพธ์ต้า

        extra.card.decrease(5); //ต้าโดนเพื่อนกินการ์ดไป 5 ใบ โดย จะใช้ Object ต้า ให้ไปเรียก Object card แล้วให้ Object card ไปเรียก method decrease เพื่อลดจำนวนการ์ดลง
        extra.money.decrease(); //ต้าทำเงินหายทั้งหมด โดยไม่มี พารามิเตอร์ (Overload method เหมือนกันกับบรรทัดที่ 34) โดยให้ Object extra ไปเรียกใช้ Object money แล้วให้ Object money ไปเรียกใช้ method decrease เพื่อลดค่าการ์ดลงทั้งหมด
        extra.Hello(); //แสดงผลลัพธ์ต้า

        yaisa.seizeCard(); //ยายสายึดการ์ดต้าทั้งหมด
        extra.Hello(); //แสดงผลลัพท์ต้า
        yaisa.Hello(); //แสดงผลลัพท์ยายสา
        
        yaisa.getMoneyYaisa(); // ยายสาโดนโจรปล้นบ้านโดยขโมยเงินยายสาไปทั้งหมด
        yaisa.Hello(); //แสดงผลลัพธ์ยายสา
        Enter();

        //Polymorphism Test 
        System.out.println("Polymorphism Test");
        Human hm[] = {extra, yaisa}; // ตรวจสอบ Polymorphism ของ Calss Human 
        for (int i = 0; i < 2; i++) { 
            if (hm[i] instanceof Human) { 
                System.out.println(hm[i].name+" instanceof Human: "+(hm[i] instanceof Human)); 
            }if(hm[i] instanceof Object){
                 System.out.println(hm[i].name+" instanceof Object: "+(hm[i] instanceof Object));
            }if(hm[i] instanceof Extra){ // ถ้าเป็น Instanceof ของ extra จริงต้องสามารถหลับในห้องเรียนได้
                 System.out.println(hm[i].name+" instanceof Extra: "+(hm[i] instanceof Extra));
                 ((Extra)hm[i]).slepping();
            }else{
                  System.out.println(hm[i].name+" instanceof Extra: "+(hm[i] instanceof Extra));
            }
            if(hm[i] instanceof Yaisa){ //ถ้าเป็นยายสาจริงต้องสามารถบ่นได้
                 System.out.println(hm[i].name+" instanceof Yaisa: "+(hm[i] instanceof Yaisa));
                 ((Yaisa)hm[i]).Complaning();
            }else{
                  System.out.println(hm[i].name+" instanceof Yaisa: "+(hm[i] instanceof Yaisa));
            }
            //จากผลลัพธ์ของการรันของโปรแกรมนี้คือ จะเห็นว่า Object yaisa สามารถเป็นได้หลายรูปแบบเช่น เป็นมนุษย์ เป็นยายสา เป็น Object และ extra ก็เช่นเดียวกัน เป็นได้ทั้ง มนุษย์ เป็น extra และ Object ซึ่งเรียกคุณสมบัติเหล่านี้ว่า Polymorphism
        }
    }

    private static void Enter() {
        System.out.println("");
    }

}
