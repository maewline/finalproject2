/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.projectfinal2;

/**
 *
 * @author Arthit
 */
public class Object_ { //ประกาศ Class Object_
    protected int volume;
    
    public Object_(int volume){ //Constuctor กำหนดค่า
        this.volume=volume;
    }
    
    public int getVolume(){ //Getter Volume ของ Object
        return volume;
    }
    
    public void decrease(int x){ //ลดค่า Object ลง x หน่วย (Overload method) 
        this.volume-=x;
    }
    
    public void decrease(){ // ลดค่า Object ลงทั้งหมด (Overload method) 
        this.volume-=this.volume;
    }
    
    public void add (int x){ //เพิ่มค่า Object ขึ้น x หน่วย
        this.volume+=x;
    }
    
    
}
